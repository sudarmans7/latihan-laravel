<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return View('halaman.register');
    }
    public function welcome(Request $request){
       // dd($request->all());
       $nama = $request -> nama;
       $register = $request->bio;

       return View('halaman.welcome', compact('nama','register'));
    }
}
