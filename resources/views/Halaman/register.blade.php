@extends('layout.master')

@section('judul')
Buat Account Baru
Register Sahabat
@endsection

@section('content')
<form action="welcome" method="POST">
    @csrf
        <label>Nama Lengkap : </label> <br>
        <input type="text" name="nama"> <br> <br>
        <label>Jenis Kelamin : </label> <br>
        <input type="radio" name="jk"> Laki-laki <br>
        <input type="radio" name="jk"> Perempuan <br> <br>
        <label>Nationality</label> <br> <br>
        <select>
            <option>Indonesian</option>
            <option>Malaysian</option>
            <option>Singapore</option>
        </select> <br> <br>
        <label> Language Spoken :</label> <br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br>
        <label>Bio : </label> <br> <br>
        <textarea name="bio" cols="40" rows="6"></textarea> <br> <br>
        <input type="submit" value="Register">
</form>
@endsection
